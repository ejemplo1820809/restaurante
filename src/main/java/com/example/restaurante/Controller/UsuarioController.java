package com.example.restaurante.Controller;

import com.example.restaurante.DTO.UsuarioDTO;
import com.example.restaurante.Entity.Usuario;
import com.example.restaurante.Mapper.DTOUsuario;
import com.example.restaurante.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private DTOUsuario mapperUsuario;


    @PostMapping("/add")
    public ResponseEntity<Usuario> save(@Valid @RequestBody UsuarioDTO usuarioDTO){
        Usuario usuario=mapperUsuario.convertirUsuario(usuarioDTO);
        Usuario usuarioAlmacenado=usuarioService.save(usuario);
        return ResponseEntity.status(HttpStatus.OK).body(usuarioAlmacenado);
    }

    @GetMapping("/listar")
    public ResponseEntity<List<String>> findAll(){
        List<Usuario> usuarios= usuarioService.findAll();
        List<String> informacionUsuario= usuarios.stream()
                .map(Usuario::getNombre).toList();
        return ResponseEntity.status(HttpStatus.OK).body(informacionUsuario);
    }

    @GetMapping("/buscar/{uuid}")
    public ResponseEntity<Usuario> findByID(@PathVariable UUID uuid){
        Usuario usuario=usuarioService.findById(uuid);
        return ResponseEntity.status(HttpStatus.OK).body(usuario);
    }

    @GetMapping("/agregarCarrito")
    public ResponseEntity<String> addProductoCarrito(@RequestParam UUID ideProducto, @RequestParam UUID ide,  @RequestParam int cantidadComprar){
        System.out.println("Este es el ide del producto"+ideProducto);
        System.out.println("Este es el ide del usuario"+ide);
        System.out.println("Esta es la cantidad a comprar"+cantidadComprar);
        try {
            int cantidad=usuarioService.addProductoCarrito(ideProducto, ide,cantidadComprar);
            System.out.println("Entro de forma correcta");
            return ResponseEntity.status(HttpStatus.OK).body("Se anadió de forma correcta al carrito"+cantidad);
        }catch (RuntimeException runtimeException){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(runtimeException.getMessage());
        }
    }

}
