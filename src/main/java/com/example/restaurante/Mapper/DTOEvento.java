package com.example.restaurante.Mapper;

import com.example.restaurante.DTO.EventoDTO;
import com.example.restaurante.Entity.Evento;
import org.springframework.stereotype.Component;

@Component
public class DTOEvento {

    public Evento convertirEventoDTO(EventoDTO dtoEvento){
        Evento evento=new Evento();
        evento.setNombreEvento(dtoEvento.getNombre());
        evento.setFechaEvento(dtoEvento.getFechaEvento());
        evento.setPrecio(dtoEvento.getPrecio());
        return evento;
    }
}
