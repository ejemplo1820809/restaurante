package com.example.restaurante.Entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="evento")
@Entity
public class Evento implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ideEvento;

    @Column(name="nombre", nullable = false, unique = true)
    private String nombreEvento;

    @DateTimeFormat(pattern = "yyyy-dd-mm")
    private Date fechaEvento;

    @Column(name="precio", nullable = false)
    private double precio;

    @ManyToMany
    @JoinTable(name="personaEvento", joinColumns = @JoinColumn(name="ideEvento"), inverseJoinColumns = @JoinColumn(name="ide"))
    private List<Persona> personaList=new ArrayList<>();

    public Evento(String nombreEvento, Date fechaEvento, double precio, String correo) {
        this.nombreEvento = nombreEvento;
        this.fechaEvento = fechaEvento;
        this.precio = precio;
    }
}




