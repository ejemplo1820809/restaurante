package com.example.restaurante.Service;


import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;

import java.util.List;
import java.util.UUID;

public interface ICategoriaService {

    //guardar categoria
    Categoria save(Categoria categoria);

    //eliminar una categoria por el id
    void deleteById(UUID ide);

    //eliminar todas las categorias
    void deleteAll();

    //buscar categoria dado un id
    Categoria findById(UUID ide);
    //Actualizar una categoria
    Categoria update(UUID uuid, Categoria categoria);

   //Listar categorias
    List<Categoria> findAll();

    List<Plato> listadoPlatos(String nombre);
}
