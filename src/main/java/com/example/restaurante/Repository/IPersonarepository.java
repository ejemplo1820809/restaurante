package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IPersonarepository extends JpaRepository<Persona, UUID> {
}
