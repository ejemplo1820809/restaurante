package com.example.restaurante.Entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="hotel")
@Entity
//Definiendo una consulta nombrada
@NamedQuery(name = "Hotel.findByCategoria", query="Select h from Hotel h where h.categoria=: name")
public class Hotel implements Serializable {
    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column (name="nombre", nullable = false, unique = true, length = 255)
    private String nombre;

    @Column (name="categoria", nullable = false, length = 255)
    private String categoria;

    @Column (name="piscina", nullable = false)
    private boolean piscina;

    @Column (name="localidad", nullable = false)
    private String localidad;

    @OneToMany(mappedBy = "hotel")
    @JsonManagedReference
    private List<Habitacion> habitaciones=new ArrayList<>();
}
