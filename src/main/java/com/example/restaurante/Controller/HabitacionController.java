package com.example.restaurante.Controller;


import com.example.restaurante.Entity.Habitacion;
import com.example.restaurante.Service.IHabitacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/habitacion")
public class HabitacionController {

    @Autowired
    private IHabitacionService ihabitacion;


    @PostMapping("/add")
    public ResponseEntity<Habitacion> save(@RequestBody Habitacion habitacion){
        Habitacion habitacions= ihabitacion.save(habitacion);
        return ResponseEntity.status(HttpStatus.OK).body(habitacions);
    }

}
