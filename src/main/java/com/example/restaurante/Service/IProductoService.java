package com.example.restaurante.Service;

import com.example.restaurante.Entity.Producto;

import java.util.UUID;

public interface IProductoService {

    public Producto save(Producto producto);

    public Producto findByID(UUID ide);
}
