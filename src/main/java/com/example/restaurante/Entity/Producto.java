package com.example.restaurante.Entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="productos")
@Entity
public class Producto implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(name="nombre", nullable = false, unique = true)
    private String nombre;

    @Column(name="precio", nullable = false)
    private float precio;

    @Column(name="cantidadDisponible", nullable = false)
    private int cantidadDisponible;

    @Column(name="cantidadComprar")
    private int cantidadComprar=0;

    @ManyToMany(mappedBy = "productos")
    List<Usuario> usuarioList;

}
