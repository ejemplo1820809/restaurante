package com.example.restaurante.Entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="habitacion")
@Entity
public class Habitacion implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(nullable = false)
    private int tamano;

    @Column(nullable = false)
    private double precio;

    @Column(nullable = false)
    private boolean desayuno;

    @Column(nullable = false)
    private boolean ocupada;

    @ManyToOne
    @JoinColumn(name = "ideHotel")
    @JsonBackReference
    private Hotel hotel;

}
