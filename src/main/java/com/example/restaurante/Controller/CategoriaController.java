package com.example.restaurante.Controller;

import org.springframework.web.bind.annotation.*;
import com.example.restaurante.DTO.CategoriaDTO;
import com.example.restaurante.DTO.PlatoDTO;
import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;
import com.example.restaurante.Mapper.DtoCategoria;
import com.example.restaurante.Mapper.DtoPlatos;
import com.example.restaurante.Service.ICategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RequestMapping("/categoria")
@RestController
public class CategoriaController {

    @Autowired
    private ICategoriaService iCategoriaService;

    @Autowired
    private DtoCategoria dtoCategoria;

    @Autowired
    private DtoPlatos dtoPlatos;

    //insertar categoria
    @PostMapping("/add")
    public ResponseEntity<String> save(@Valid @RequestBody CategoriaDTO categoriaDTO){
        Categoria categoria=dtoCategoria.convertirCategoriaDTO(categoriaDTO);
        Categoria categoriaGuardada=iCategoriaService.save(categoria);
        return ResponseEntity.status(HttpStatus.OK).body("La categoría se guardó de forma correcta");
    }

    //mostrar categoria/editar categoria
    @GetMapping("buscarId/{ide}")
    public ResponseEntity<CategoriaDTO> findById(@PathVariable UUID ide){
        Categoria categoria=iCategoriaService.findById(ide);
        return ResponseEntity.status(HttpStatus.OK).body(dtoCategoria.convertirCategoria(categoria));
    }

    //eliminar categoria dado un ide
    @DeleteMapping ("eliminarId/{ide}")
    public ResponseEntity<String> deleteById(@PathVariable UUID ide){
        iCategoriaService.deleteById(ide);
        return ResponseEntity.status(HttpStatus.OK).body("Se eliminó de forma satisfactoria la categoria");
    }
    //eliminar todas las categorias
    @DeleteMapping("eliminarAll")
    public ResponseEntity<String> deleteAll(){
        iCategoriaService.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).body("Se eliminó de forma satisfactoria todas las categorias");
    }

    //actualizar categoria
    @PutMapping("/actualizar/{ide}")
    public ResponseEntity<String> update(@PathVariable UUID ide, @Valid @RequestBody  CategoriaDTO categoriaDTO){
        Categoria categoria=dtoCategoria.convertirCategoriaDTO(categoriaDTO);
        Categoria categoriaActualizar=iCategoriaService.update(ide, categoria);
        return ResponseEntity.status(HttpStatus.OK).body("La categoría se actulizó de forma correcta");
    }

    //listar categorias
    @GetMapping("/listar")
    public ResponseEntity<List<CategoriaDTO>> findAll(){
        List<Categoria> categorias=iCategoriaService.findAll();
        List<CategoriaDTO>categoriaDTOS=dtoCategoria.convertirListcategoria(categorias);
        for (CategoriaDTO ca: categoriaDTOS) {
            System.out.println(ca.getNombreCategoria()+ca.getDescripcionCategoria());
        }
        return ResponseEntity.status(HttpStatus.OK).body(categoriaDTOS);
    }

    @GetMapping("listar/platos")
    public ResponseEntity<List<PlatoDTO>> findCategoriaPlatos(@RequestParam String nombreCategoria){
        List<Plato> listadoPlatos=iCategoriaService.listadoPlatos(nombreCategoria);
        List<PlatoDTO> listadoPlatosDTO=dtoPlatos.convertirListPlatoCategoria(listadoPlatos);
        return ResponseEntity.status(HttpStatus.OK).body(listadoPlatosDTO);
    }
}
