package com.example.restaurante.Controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value= RuntimeException.class)
    public ResponseEntity<Error> runtimeExceptionHandler(RuntimeException runtimeException){
        Error errorDtO= Error.builder().codigo("P.500").mensaje(runtimeException.getMessage()).build();
        return new ResponseEntity<>(errorDtO, HttpStatus.BAD_REQUEST);
    }

}
