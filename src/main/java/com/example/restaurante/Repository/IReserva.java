package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Reserva;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;



public interface IReserva extends JpaRepository<Reserva, UUID> {

    List<Reserva> findByDateAndHoraEspecifica(Date fecha, LocalTime time);
}
