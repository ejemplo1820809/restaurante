package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IProducto extends JpaRepository<Producto, UUID> {
}
