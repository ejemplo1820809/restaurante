package com.example.restaurante.Service;

import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;

import java.util.List;
import java.util.UUID;

public interface IPlatoService {

    Plato save(Plato plato);

    Plato update(UUID ide, Plato plato);

    List<Plato> findAll();

    Plato findById(UUID uuid);

    void deleteAll();

    void deleteById(UUID ide);

    List<Plato> findByCategoria(String nombre);

    List<Plato> findByVegetariana(boolean vegetariano);

    List<Plato> findBtActivo(boolean activo);

    byte[] CrearCarta() throws Exception;

}
