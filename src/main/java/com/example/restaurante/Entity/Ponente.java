package com.example.restaurante.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@DiscriminatorValue("ponente")
@Entity
public class Ponente extends Persona{

    @Column(name="titulo", nullable = false, unique = false)
    protected String tituloPonencia;
}
