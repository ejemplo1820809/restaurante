package com.example.restaurante.Mapper;

import com.example.restaurante.DTO.CategoriaDTO;
import com.example.restaurante.Entity.Categoria;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DtoCategoria {

    //Convertir del DTO a una Entidad
    public Categoria convertirCategoriaDTO(CategoriaDTO categoriaDTO){
        Categoria categoria=new Categoria();
        categoria.setNombreCategoria(categoriaDTO.getNombreCategoria());
        if(categoriaDTO.getDescripcionCategoria()==null){
            categoria.setDescripcionCategoria(null);
        }else{
            categoria.setDescripcionCategoria(categoriaDTO.getDescripcionCategoria());
        }
        return categoria;
    }

    //Convertir de una entidad a un DTO
    public CategoriaDTO convertirCategoria(Categoria categoria){
        CategoriaDTO categoriaDTO=new CategoriaDTO();
        categoriaDTO.setNombreCategoria(categoria.getNombreCategoria());
        if(categoria.getDescripcionCategoria()==null){
            categoriaDTO.setDescripcionCategoria(null);
        }else{
            categoriaDTO.setDescripcionCategoria(categoria.getDescripcionCategoria());
        }
        return categoriaDTO;
    }

    public List<CategoriaDTO> convertirListcategoria(List<Categoria> categorias){
        List<CategoriaDTO> listCategoriasDTO=new ArrayList<>();
        for (Categoria categoria: categorias) {
            CategoriaDTO categoriaDTO=new CategoriaDTO();
            categoriaDTO.setNombreCategoria(categoria.getNombreCategoria());
            if(categoria.getDescripcionCategoria()==null){
                categoriaDTO.setDescripcionCategoria(null);
            }else{
                categoriaDTO.setDescripcionCategoria(categoria.getDescripcionCategoria());
            }
            listCategoriasDTO.add(categoriaDTO);
        }
        return listCategoriasDTO;
    }
}
