package com.example.restaurante.Service;

import com.example.restaurante.Entity.Evento;

import java.io.IOException;
import java.util.UUID;

public interface IEventoService {

    public Evento save(Evento evento);

    public Evento findElementByID(UUID uuid);

    public Evento findElementByName(String name);

    public int  cantidadParticipantes(String name);

    public int cantidadParticipantesPonentes(String name);

    public double dineroRecaudado(String name);

    public String deleteById(String name);

    public byte[] creandoExcelInformacion(String name) throws IOException;

}
