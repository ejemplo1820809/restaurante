package com.example.restaurante.Entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="platos")
@Entity
public class Plato implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(name="nombre", unique = true, length = 255, nullable = false)
    private String nombre;

    @Column(name="ingredientes", nullable = false)
    private String Ingredientes;

    @Column(name="precio", nullable = false)
    private double precioReal;

    @Column(name="descuento")
    private int descuento=0;

    @Column(name="activo", nullable = false)
    private boolean activo;

    @Column (name="vegetariano", nullable = false)
    private boolean vegetariano;

    @ManyToOne
    private Categoria categoria;

    @NotNull
    private byte[]imagen;

    public double PrecioCarta(){
        double precioCarta=0;
        if(this.descuento==0){
            return precioCarta;
        }
        else{
            precioCarta=(this.precioReal*this.descuento)/100;
            return precioCarta;
        }
    }

}
