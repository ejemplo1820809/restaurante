package com.example.restaurante.Service;


import com.example.restaurante.Entity.Habitacion;
import com.example.restaurante.Repository.IHabitacionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HabitacionService implements IHabitacionService {

    @Autowired
    private IHabitacionRepository  habitacionRepository;

    public Habitacion save(Habitacion habitacion){
        return habitacionRepository.save(habitacion);
    }



}
