package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface IEventorepository extends JpaRepository<Evento, UUID> {

    @Query("select e From Evento  e where e.fechaEvento between :fechaUno and :fechaDos" )
    public List<Evento> listadoEventos(@Param("fechaUno") Date fechaUno, @Param("fechaDos") Date fechaDos);

    @Query("Select  e.fechaEvento, count(e) From Evento e group by  e.fechaEvento")
    public List<Object[]> cantiadadEventosFecha();

    public Evento findByNombreEvento(String name);


}
