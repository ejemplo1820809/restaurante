package com.example.restaurante;

import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Repository.ICategoria;
import com.example.restaurante.Service.CategoriaService;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TestCategoria {



    //probando el metodo de findByID
    @Test
    public void TestFindByIDGategoria(){
        //Mockito se utiliza para crear objetos simulados de clases e interfaces aqui estoy mokeando la interfaz del repositorio
       ICategoria iCategoria=mock(ICategoria.class);
       //Aqui estoy creando un id en formato de texto
       UUID id=UUID.fromString("e0b5ade5-3a84-420a-b5b2-63754e4662fc");
       //Creando una categoria
       Categoria categoria=new Categoria(id, "Carnes", null, null);
       //creando un optional que le paso una categoria
       Optional<Categoria> optionalCategoria=Optional.of(categoria);
       when(iCategoria.findById(id)).thenReturn(optionalCategoria);

       //Creando una instancia de categoria service le paso una categoria
       CategoriaService categoriaService=new CategoriaService(iCategoria);

       Categoria categoria1=categoriaService.findById(id);
       assertEquals("Carnes", categoria1.getNombreCategoria());

       verify(iCategoria, times(1)).findById(id);
    }

    //probando el metodo salvar
    @Test
    public void testSave(){
      //  UUID ide=UUID.fromString("e0b5ade5-3a84-420a-b5b2-63754e4662fc");
        ICategoria iCategoria=mock(ICategoria.class);
        Categoria categoria= new Categoria("Pastas", null);
        when(iCategoria.save(categoria)).thenReturn(categoria);

        CategoriaService categoriaService=new CategoriaService(iCategoria);
        Categoria categoria1=categoriaService.save(categoria);
        assertEquals("Pastas", categoria1.getNombreCategoria());
    }

    //Salvar
    @Test
    public void testSaveIncompleto(){
        //  UUID ide=UUID.fromString("e0b5ade5-3a84-420a-b5b2-63754e4662fc");
        ICategoria iCategoria=mock(ICategoria.class);
        Categoria categoria= new Categoria(null, null);
        //Configurar para lanzar una excepcion
        doThrow(new RuntimeException("El nombre de la categoría no puede ser nulo")).when(iCategoria).save(categoria);

        CategoriaService categoriaService=new CategoriaService(iCategoria);
        assertThrows(RuntimeException.class, () -> {
            categoriaService.save(categoria);
        });
    }

    @Test
    public void testFindAll(){
        Categoria categoriaUno=new Categoria("Carnes", "es usada para");
        Categoria categoriaDos=new Categoria( "Postres", "Los platos dulces son");
        List<Categoria> listado = new ArrayList<>();
        listado.add(categoriaUno);
        listado.add(categoriaDos);
        ICategoria categoria=mock(ICategoria.class);
        when(categoria.findAll()).thenReturn(listado);

        //Convirtiendo arreglos
        //arreglo q yo cree
        Categoria[]categoriasDefinidas=listado.toArray(new Categoria[listado.size()]);
        CategoriaService categoriaService=new CategoriaService(categoria);
        //arreglo que me va a devolver al llamar al metodo
        List<Categoria>categoriasObtenidas=categoriaService.findAll();
        Categoria[]categoriasConvertidas=categoriasObtenidas.toArray(new Categoria[categoriasObtenidas.size()]);

        assertArrayEquals(categoriasDefinidas, categoriasConvertidas);

    }

}
