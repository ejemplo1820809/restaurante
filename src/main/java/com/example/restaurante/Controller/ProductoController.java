package com.example.restaurante.Controller;


import com.example.restaurante.DTO.ProductoDTO;
import com.example.restaurante.Entity.Producto;
import com.example.restaurante.Mapper.DTOProducto;
import com.example.restaurante.Service.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


@RestController
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private IProductoService iProductoService;

    @Autowired
    private DTOProducto mapperProducto;


    @PostMapping("/add")
    public ResponseEntity<Producto> save(@Valid @RequestBody ProductoDTO productoDTO){
        Producto producto= mapperProducto.convertirProducto(productoDTO);
        Producto productoSalvado=iProductoService.save(producto);
        return ResponseEntity.status(HttpStatus.OK).body(productoSalvado);
    }


}
