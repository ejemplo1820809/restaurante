package com.example.restaurante.Entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.internal.build.AllowPrintStacktrace;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="categoria")
@Entity
public class Categoria implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID id;

    @Column(name = "nombre", unique = true, nullable = false, length = 255)
    private String nombreCategoria;


    @Column(name="descripcion", nullable = true)
    private String descripcionCategoria;

    //ver esto con llull
    @OneToMany(mappedBy = "categoria", cascade = CascadeType.REMOVE)
    private List<Plato> platos;

    public Categoria(String nombreCategoria, String descripcionCategoria) {
        this.nombreCategoria = nombreCategoria;
        this.descripcionCategoria = descripcionCategoria;
    }
}
