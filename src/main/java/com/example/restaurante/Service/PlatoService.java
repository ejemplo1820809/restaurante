package com.example.restaurante.Service;


import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;
import com.example.restaurante.Repository.ICategoria;
import com.example.restaurante.Repository.IPlato;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PlatoService implements IPlatoService {

    @Autowired
    private IPlato iPlato;

    @Autowired
    private ICategoria iCategoria;

    public Plato save(Plato plato){
        return iPlato.save(plato);
    }

    public List<Plato> findAll(){
        List<Plato> listadoPlatos=iPlato.findAll();
        if(listadoPlatos.isEmpty()){
            throw new RuntimeException("No hay platos agregados");
        }
        return listadoPlatos;
    }

    public Plato findById(UUID ide){
        Optional<Plato> plato=iPlato.findById(ide);
        if(!plato.isPresent()){
            throw new RuntimeException("EL plato con el ide proporcionado no existe");
        }
        return plato.get();
    }

    public void deleteAll(){
        iPlato.deleteAll();
    }

    public void deleteById(UUID ide){
        Plato plato=findById(ide);
        iPlato.deleteById(plato.getIde());
    }

   public List<Plato> findByCategoria(String nombre){
        List<Plato> platos=iPlato.findByCategoria_NombreCategoria(nombre);
        if(platos.isEmpty()){
            throw new RuntimeException("No hay platos agregados con la categoria seleccionada");
        }
        return platos;
   }

   public List<Plato> findByVegetariana(boolean vegetariano){
       List<Plato> platos=iPlato.findByVegetariano(vegetariano);
       if(platos.isEmpty()){
           throw new RuntimeException("No hay platos con el criterio seleccionado");
       }
       return platos;
   }

    public List<Plato> findBtActivo(boolean activo){
        List<Plato> platos=iPlato.findByActivo(activo);
        if(platos.isEmpty()){
            throw new RuntimeException("No hay platos con el criterio seleccionado");
        }
        return platos;
    }

    public Plato update(UUID ide, Plato plato){
        Plato platoBuscado=findById(ide);
        platoBuscado.setIngredientes(plato.getIngredientes());//ingredientes
        platoBuscado.setPrecioReal(plato.getPrecioReal());//precio real
        platoBuscado.setDescuento(plato.getDescuento());//descuento
        platoBuscado.setImagen(plato.getImagen());//imagen
        platoBuscado.setVegetariano(plato.isVegetariano());//es vegetariano
        platoBuscado.setActivo(plato.isActivo());//es activo
        platoBuscado.setCategoria(plato.getCategoria());//categoria
        return iPlato.save(platoBuscado);
    }

    public byte[] CrearCarta() throws Exception {
        //Formatos para los textos
        Font titulo = FontFactory.getFont(FontFactory.TIMES_ROMAN, 26, Font.BOLD);
        Font fecha = FontFactory.getFont(FontFactory.TIMES_ROMAN, 18, Font.BOLD);
        //Obtener la fecha actual
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
        String fechaActual=simpleDateFormat.format(new Date());

        //Creando el documento
        Document documento = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter.getInstance(documento, baos);

        //Abrir el documento
        documento.open();

        //Un chunk es un String
        Paragraph paragraphTitulo=new Paragraph();
        Paragraph paragraphFecha=new Paragraph();
        Chunk chunkTitle = new Chunk("Nuestra Carta", titulo);
        Chunk chunkFecha=new Chunk("Fecha:"+ fechaActual, fecha);
        Chunk chunkSaltoLinea = new Chunk(Chunk.NEWLINE);

        //Seccion uno
        paragraphTitulo.add(chunkTitle);
        paragraphTitulo.setAlignment(Element.ALIGN_CENTER);
        paragraphFecha.add(chunkFecha);
        paragraphFecha.setAlignment(Element.ALIGN_CENTER);
        documento.add(paragraphTitulo);
        documento.add(chunkSaltoLinea);
        documento.add(paragraphFecha);

        String iTextExampleImage = "C:\\Users\\Llull\\Desktop\\fotos restaurante\\menu.jpg";
        Image image;
        try {
            image = Image.getInstance(iTextExampleImage);
            image.setAbsolutePosition(200, 450);
            documento.add(image);
        } catch (BadElementException ex) {
            System.out.println("Image BadElementException" +  ex);
        } catch (IOException ex) {
            System.out.println("Image IOException " +  ex);
        }
        //documento.add(chapter);

        //Creando la seccion dos
        Chapter chapterDos=new Chapter(2);
        Section section= chapterDos.addSection("Nuestros platos", 1);
        List<Categoria> listaCategorias=iCategoria.findByPlatosIsNotNull();
        int cantidadCategorias=listaCategorias.size();
        for (Categoria listaCategoria : listaCategorias) {
            Chunk chunkNombreCategoria = new Chunk(listaCategoria.getNombreCategoria());
            section.add(chunkNombreCategoria);
            PdfPTable table = new PdfPTable(5);
            PdfPCell header1 = new PdfPCell(new Paragraph("Nombre"));
            PdfPCell header2 = new PdfPCell(new Paragraph("Ingredientes"));
            PdfPCell header3 = new PdfPCell(new Paragraph("Precio actual con descuento"));
            PdfPCell header4 = new PdfPCell(new Paragraph("Vegetariano"));
            PdfPCell header5 = new PdfPCell(new Paragraph("Imagen"));

            table.addCell(header1);
            table.addCell(header2);
            table.addCell(header3);
            table.addCell(header4);
            table.addCell(header5);
            for (int j = 0; j < listaCategoria.getPlatos().size(); j++) {
                PdfPCell cell_uno = new PdfPCell(new Paragraph(listaCategoria.getPlatos().get(j).getNombre()));
                PdfPCell cell_dos = new PdfPCell(new Paragraph(listaCategoria.getPlatos().get(j).getIngredientes()));
                double precio=listaCategoria.getPlatos().get(j).getPrecioReal();
                System.out.println("Este es el precio"+ precio);
                int descuento=listaCategoria.getPlatos().get(j).getDescuento();
                System.out.println("Este es el descuento"+ descuento);
                double precioCarta=0;
                if(descuento==0){
                    precioCarta=precio;
                }else{
                    precioCarta=(precio*descuento)/100;
                }
                System.out.println("Este es el precio que se debe mostrar en la carta"+precioCarta);
                String precioConvertido=String.valueOf(precioCarta);
                PdfPCell cell_tres = new PdfPCell(new Paragraph(precioConvertido));
                PdfPCell cell_cuatro = new PdfPCell(new Paragraph(String.valueOf(listaCategoria.getPlatos().get(j).isVegetariano())));
                byte[]imagen=listaCategoria.getPlatos().get(j).getImagen();
                Image image_act=Image.getInstance(imagen);
                PdfPCell cell_cinco = new PdfPCell();
                cell_cinco.addElement(image_act);
                table.addCell(cell_uno);
                table.addCell(cell_dos);
                table.addCell(cell_tres);
                table.addCell(cell_cuatro);
                table.addCell(cell_cinco);
            }
            section.add(table);

        }
        documento.add(chapterDos);
        documento.close();
        byte[] pdfBytes = baos.toByteArray();
        return pdfBytes;

    }

}
