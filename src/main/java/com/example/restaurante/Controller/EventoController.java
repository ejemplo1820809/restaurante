package com.example.restaurante.Controller;


import com.example.restaurante.DTO.EventoDTO;
import com.example.restaurante.Entity.Evento;
import com.example.restaurante.Mapper.DTOEvento;
import com.example.restaurante.Service.IEventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;


@RequestMapping("/evento")
@RestController
public class EventoController {

    @Autowired
    private IEventoService ieventoService;

    @Autowired
    private DTOEvento dtoEvento;


    @PostMapping("/add")
    public ResponseEntity<Evento> save(@Valid @RequestBody EventoDTO eventoDTO){
        Evento evento=dtoEvento.convertirEventoDTO(eventoDTO);
        Evento eventoAlmacenado=ieventoService.save(evento);
        return ResponseEntity.status(HttpStatus.OK).body(evento);
    }

    @GetMapping("/buscarId/{ide}")
    public ResponseEntity<HashMap<String, Date>> findElementByID(@PathVariable UUID ide){
        Evento evento=ieventoService.findElementByID(ide);
        HashMap<String, Date> informacion=new HashMap<>();
        informacion.put(evento.getNombreEvento(), evento.getFechaEvento());
        return ResponseEntity.status(HttpStatus.OK).body(informacion);
    }

    @GetMapping("/buscarName/{name}")
    public ResponseEntity<HashMap<String, Date>> findElementByName(@PathVariable String name){
        Evento evento=ieventoService.findElementByName(name);
        HashMap<String, Date> informacion=new HashMap<>();
        informacion.put(evento.getNombreEvento(), evento.getFechaEvento());
        return ResponseEntity.status(HttpStatus.OK).body(informacion);
    }

    @GetMapping("/cantidad/{name}")
    public ResponseEntity<Integer> cantidadParticipantes(@PathVariable String name){
        int cantidadParticipantes=ieventoService.cantidadParticipantes(name);
        return ResponseEntity.status(HttpStatus.OK).body(cantidadParticipantes);
    }

    @GetMapping("/cantidadPonentes/{name}")
    public ResponseEntity<Integer> cantidadParticipantesPonentes(@PathVariable String name){
        int cantidadParticipantesPonentes=ieventoService.cantidadParticipantesPonentes(name);
        return ResponseEntity.status(HttpStatus.OK).body(cantidadParticipantesPonentes);
    }

    @GetMapping("/recaudacion/{name}")
    public ResponseEntity<Double> dineroRecaudado(@PathVariable String name){
        double dineroRecaudado=ieventoService.dineroRecaudado(name);
        return ResponseEntity.status(HttpStatus.OK).body(dineroRecaudado);
    }

    @GetMapping("/descargar_excel/{name}")
    public ResponseEntity<byte[]> descargarExcel(@PathVariable String name) throws IOException {
        byte[] excelBytes = ieventoService.creandoExcelInformacion(name);
        // Configurar los encabezados de la respuesta HTTP
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("filename", "archivo_descargado.xlsx");
        return ResponseEntity.ok()
                .headers(headers)
                .body(excelBytes);
    }




}
