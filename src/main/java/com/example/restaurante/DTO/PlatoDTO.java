package com.example.restaurante.DTO;


import com.example.restaurante.Entity.Categoria;
import jakarta.persistence.Column;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PlatoDTO {

    @NotNull
    private String nombre;

    @NotNull
    private String ingredientes;

    @NotNull
    private double precioReal;

    @NotNull
    private int descuento;

    @NotNull
    private boolean activo;

    @NotNull
    private boolean vegetariano;

    @NotNull
    private String nombreCategoria;

    private byte[]imagen;

    public PlatoDTO(String nombre, String ingredientes, double precioReal, int descuento, boolean activo, boolean vegetariano, byte[] imagen) {
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.precioReal = precioReal;
        this.descuento = descuento;
        this.activo = activo;
        this.vegetariano = vegetariano;
        this.imagen = imagen;
    }
}
