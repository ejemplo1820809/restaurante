package com.example.restaurante.Service;

import com.example.restaurante.Entity.Evento;
import com.example.restaurante.Entity.Persona;
import com.example.restaurante.Repository.IEventorepository;
import com.example.restaurante.Repository.IPersonarepository;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class PersonaService implements IPersonaService{

    @Autowired
    private IPersonarepository iPersonarepository;

    @Autowired
    private IEventorepository iEventorepository;

    @Autowired
    private JavaMailSender javaMailSender;

    public void enviarCorreo(String destinatario, String asunto, String cuerpo) {
        MimeMessage mensaje = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mensaje);
        try {
            helper.setTo(destinatario);
            helper.setSubject(asunto);
            helper.setText(cuerpo, true); // true indica que el cuerpo es HTML

            javaMailSender.send(mensaje);
        }
        catch (jakarta.mail.MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    public Persona save(Persona persona){
        String nombreEvento=persona.getNombreEvento();
        System.out.println("Este es el nombre del evento"+nombreEvento);
        Evento evento=iEventorepository.findByNombreEvento(nombreEvento);
        System.out.println("Este es el evento una vez buscado en la BD"+evento.getNombreEvento());
        Persona personaAlmacenada=iPersonarepository.save(persona);
        personaAlmacenada.getEventoList().add(evento);
        iPersonarepository.save(personaAlmacenada);
        String correo=personaAlmacenada.getCorreo();
        enviarCorreo(correo, "Bienvenida "+ personaAlmacenada.getNombre()+ "se inscribio en el evento"+ evento.getNombreEvento(), "Persona");
        return  personaAlmacenada;
    }
}
