package com.example.restaurante.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalTime;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ReservaDTO {

    @NotNull
    private String nombre;

    @Email
    @NotNull
    private String correoElectronico;

    @NotNull
    private int numeroTelefono;

    @NotNull
    private String direccion;

    @NotNull
    private int cantidadPersonas;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date date;

    @DateTimeFormat(pattern = "HH:mm:ss")
    @NotNull
    private LocalTime hora;

    private String estado;

    private int numeroMesa;

}
