package com.example.restaurante.Controller;


import com.example.restaurante.Entity.Habitacion;
import com.example.restaurante.Entity.Hotel;
import com.example.restaurante.Service.IHotelService;
import com.google.rpc.context.AttributeContext;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RequestMapping("hoteles")
@RestController
public class HotelController {

    @Autowired
    private IHotelService hotelService;

    @PostMapping("/add")
    public ResponseEntity<Hotel> save(@RequestBody Hotel hotel){
        Hotel hotelA=hotelService.save(hotel);
        return ResponseEntity.status(HttpStatus.OK).body(hotelA);
    }

    @GetMapping("/listar")
    public ResponseEntity<Page<Hotel>> listarHoteles(@RequestParam("pagina") int pagina, @RequestParam("cantidad") int cantidad){
       Page <Hotel> page= hotelService.findAll(pagina, cantidad);
        return ResponseEntity.status(HttpStatus.OK).body(page);

    }

    @GetMapping("/ordenar")
    public ResponseEntity<List<Hotel>> ordenarHoteles(){
        List<Hotel> listado= hotelService.obtenerHotelOrdenado();
        return ResponseEntity.status(HttpStatus.OK).body(listado);
    }

    @PutMapping("/actualizar")
    public ResponseEntity<String> Asignar(@RequestParam("idHabitacion") UUID idHabitacion,
                                          @RequestParam("nombreHotel") String nombreHotel){
        hotelService.AsignarHabitacion(idHabitacion, nombreHotel);
        return ResponseEntity.ok("operacion exitosa");

    }

    @GetMapping("/listarrN/{name}")
    public ResponseEntity<List<Habitacion>> listarHabitaciones(@PathVariable String name){
        List<Habitacion> listado= hotelService.listadohabitaciones(name);
        return ResponseEntity.status(HttpStatus.OK).body(listado);
    }


}
