package com.example.restaurante.Service;


import com.example.restaurante.Entity.Reserva;
import com.example.restaurante.Repository.IReserva;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class Reservaservice implements IReservaService{

    @Autowired
    private IReserva iReserva;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private WhatAppService whatAppService;

    public void enviarCorreo(String destinatario, String asunto, String cuerpo) {
        MimeMessage mensaje = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mensaje);

        try {
            helper.setTo(destinatario);
            helper.setSubject(asunto);
            helper.setText(cuerpo, true); // true indica que el cuerpo es HTML

            javaMailSender.send(mensaje);
        }
        catch (jakarta.mail.MessagingException e) {
            throw new RuntimeException(e);
        }

    }
    public List<Reserva> findAll(){

        return iReserva.findAll();
    }

    public Reserva save(Reserva reserva){
       List<Reserva> reservaA=iReserva.findByDateAndHoraEspecifica(reserva.getDate(), reserva.getHoraEspecifica());
       System.out.println("esta es la cantidad de reservas que hay"+ reservaA.size());
        if(reservaA.size()>5){
            throw new RuntimeException("No se puede realizar la reservacion debe seleccionar otro horario. En ese horario estan ocupadas todas esas mesas");
        }
        Reserva reservaGuardada= iReserva.save(reserva);
        Date date=reservaGuardada.getDate();
        enviarCorreo(reservaGuardada.getEmail(), "Usted realizó una reserva en el restaurante D'family",
                "Usted ha realizado una reservación en nuestro restaurante por esta via le confirmaremos su solicitud de reserva para la fecha "+ date);
        whatAppService.enviarMensaje(54689342, reservaGuardada.getNombreCompleto(), reservaGuardada.getDate());
        return reservaGuardada;
    }
}
