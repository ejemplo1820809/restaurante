package com.example.restaurante.DTO;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PersonaDTO {
    @NotNull
    private String nombre;

    @NotNull
    private String primerApellido;

    @NotNull
    private String solapin;

    @NotNull
    private String institucion;

    @Email
    @NotNull
    private String correo;

    private String tituloPonencia;

    @NotNull
    private String nombreEvento;


}
