package com.example.restaurante.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.internal.build.AllowPrintStacktrace;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class EventoDTO {
    @NotNull
    private String nombre;

    @DateTimeFormat(pattern = "yyyy-dd-mm")
    private Date fechaEvento;

    @NotNull
    private double precio;
}
