package com.example.restaurante.Service;


import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Date;

@Service
public class WhatAppService {

   @Value("${twilio.account_sid}")
  private String ACCOUNT_SID;

   @Value("${twilio.auth_token}")
    private String AUTH_TOKEN;

    public void enviarMensaje(int numeroDestino, String nombreCliente, Date date) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message.creator(
                new PhoneNumber("whatsapp:" + numeroDestino),
                new PhoneNumber("whatsapp:+14155238886"),
                "Se realizó un reservación por: "+nombreCliente+"Para el dia "+ date
        ).create();
    }

}
