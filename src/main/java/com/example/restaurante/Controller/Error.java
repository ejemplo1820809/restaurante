package com.example.restaurante.Controller;


import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Error {

    private String codigo;

    private String mensaje;
}
