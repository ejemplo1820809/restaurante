package com.example.restaurante.Service;

import com.example.restaurante.Entity.Reserva;

import java.util.List;

public interface IReservaService {

    Reserva save(Reserva reserva);

    List<Reserva> findAll();

}
