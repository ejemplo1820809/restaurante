package com.example.restaurante.DTO;


import com.example.restaurante.Entity.Plato;
import jakarta.persistence.OneToMany;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class CategoriaDTO {
    @NotNull
    private String nombreCategoria;

    private String descripcionCategoria;



}
