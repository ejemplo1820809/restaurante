package com.example.restaurante.Entity;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo")
@Table(name="persona")
@Entity
public class Persona implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "primerApellido", nullable = false)
    private String primerApellido;

    @Column(name = "solapin", nullable = false, length = 5)
    private String solapin;

    @Column(name = "institucion", nullable = false)
    private String institucion;

    @Email
    @Column(name = "correo", nullable = false)
    private String  correo;

    @Transient
    private String nombreEvento;

    @ManyToMany
    @JoinTable(name="personaEvento", joinColumns = @JoinColumn(name="ide"), inverseJoinColumns = @JoinColumn(name="ideEvento"))
    private List <Evento> eventoList=new ArrayList<>();
}
