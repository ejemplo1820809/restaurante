package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Habitacion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.UUID;

public interface IHabitacionRepository extends JpaRepository<Habitacion, UUID> {

    //Filtrar habitaciones por tamano
    public List<Habitacion> findByTamano(int tamano);

    //Filtrar habitaciones por precio
    public List<Habitacion> findByPrecio(double precio);

    //Filtrar habitaciones que esten ocupadas o no
    public List<Habitacion> findByOcupada(boolean valor);

    //En este caso el valor de ocupada no hace falta porque al ser un atributo booleano
    //si no lo pongo en la firma del metodo el me va acoger los que tienen valor falso
    //en esa columna y si lo pongo en la firma cogeria a los que tienen valor true
    //en el caso de que ya vaya a depender del usuario entonces si defino un parametro
    //para evaluar lo que tiene el parametro
    public List<Habitacion> findByTamanoAndPrecioGreaterThanEqualAndPrecioLessThanEqual
            ( int tamano, double precioMin, double precioMax);



}
