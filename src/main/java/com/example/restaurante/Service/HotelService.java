package com.example.restaurante.Service;


import com.example.restaurante.Entity.Habitacion;
import com.example.restaurante.Entity.Hotel;
import com.example.restaurante.Repository.IHabitacionRepository;
import com.example.restaurante.Repository.IHotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class HotelService implements IHotelService {

    @Autowired
    private IHotelRepository hotelRepository;

    @Autowired
    private IHabitacionRepository ihabitacion;


    public Hotel save(Hotel hotel) {
        return hotelRepository.save(hotel);
    }


    //Este ejemplo es para paginar en este caso sl primer
    //parametro indica el numero de pagina que quieres obtener y el segundo
    //la cantdidad de elementos que quieres visualizar
    public Page<Hotel> findAll(int page1, int page2) {
        Pageable pageable = PageRequest.of(page1, page2);
        return hotelRepository.findAll(pageable);
    }

    public List<Hotel> obtenerHotelOrdenado() {
        Sort sort = Sort.by(Sort.Direction.ASC, "nombre");
        return hotelRepository.findAll(sort);

    }

    public List<Hotel> findByCategoria(String name) {
        List<Hotel> listado = hotelRepository.findByCategoria(name);
        if (listado.isEmpty()) {
            throw new RuntimeException("no hay hoteles en esa categoria");
        } else {
            return listado;
        }
    }

    public List<Hotel> findByLocalidad(String name) {
        List<Hotel> listado = hotelRepository.findByLocalidad(name);
        if (listado.isEmpty()) {
            throw new RuntimeException("no hay hoteles en esa localidad");
        } else {
            return listado;
        }
    }

    public void AsignarHabitacion(UUID ideHabi, String nombreHotel) {
        Optional<Habitacion> hab = ihabitacion.findById(ideHabi);
        if (!hab.isPresent()) {
            throw new RuntimeException("La habitacion no existe");
        }
        Habitacion habObtenida = hab.get();
        System.out.println(habObtenida.getPrecio());
        Hotel hotel = hotelRepository.findByNombreEquals(nombreHotel);
        if (hotel != null) {
            List<Habitacion> listado = hotel.getHabitaciones();
            listado.add(habObtenida);

            habObtenida.setHotel(hotel);
            ihabitacion.save(habObtenida);

            hotel.setHabitaciones(listado);
           Hotel hotels= hotelRepository.save(hotel);
            System.out.print("Operacion exitosa"+hotels.getHabitaciones().size());
        }


    }

    public List<Habitacion> listadohabitaciones(String name) {
        Hotel hotel = hotelRepository.findByNombreEquals(name);
        System.out.println("Este es el nomre"+hotel.getNombre());
        if (hotel != null) {
            List<Habitacion> listado = hotel.getHabitaciones();
            System.out.println("esta es la cantidad"+listado.size());
            return listado;
        } else {
                   throw new RuntimeException("No se encuentra ese hotel");
        }

    }
}
