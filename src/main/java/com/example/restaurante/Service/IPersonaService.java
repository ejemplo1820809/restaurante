package com.example.restaurante.Service;

import com.example.restaurante.Entity.Persona;

public interface IPersonaService {

    public Persona save (Persona persona);
}
