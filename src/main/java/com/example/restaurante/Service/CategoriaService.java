package com.example.restaurante.Service;


import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;
import com.example.restaurante.Repository.ICategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CategoriaService implements ICategoriaService{

    @Autowired
    private ICategoria iCategoria;


    //insertar categoria
    public Categoria save(Categoria categoria){
        return  iCategoria.save(categoria);
    }


    //listar todas las categorias
    public List<Categoria> findAll(){
        List<Categoria> categorias=iCategoria.findAll();
        if(categorias.isEmpty()){
            throw new RuntimeException("No hay categorias ingresadas");
        }else{
            return categorias;
        }
    }

    //buscar categoria dado un ide
    public Categoria findById(UUID ide){
        Optional<Categoria> categoria=iCategoria.findById(ide);
        if(!categoria.isPresent()){
            throw new RuntimeException("La categoria no existe");
        }
        return categoria.get();
    }

    //elimnar categoria dado un ide
    public void deleteById(UUID ide){
        Categoria categoria=findById(ide);
        List<Plato> platos=categoria.getPlatos();
        if(platos.isEmpty()){
            throw new RuntimeException("No se puede eliminar hay platos que tienen esa categoria asignada");
        }
        iCategoria.deleteById(ide);
    }

    //eliminar todas las categorias
    public void deleteAll(){
        List<Categoria> listadoCategorias=iCategoria.findAll();
        List<Categoria> categoriasEliminar=new ArrayList<>();
        if(listadoCategorias.isEmpty()){
            throw new RuntimeException("No hay categorias agregadas");
        }else{

            for (Categoria categoria: listadoCategorias) {
                if(categoria.getPlatos().isEmpty()){
                    categoriasEliminar.add(categoria);
                }
            }
            iCategoria.deleteAll(categoriasEliminar);
        }
    }

    //actualizar categoria
    public Categoria update(UUID uuid, Categoria categoria){
        Categoria categoriaOptenida=findById(uuid);
        if(!categoria.getNombreCategoria().equals(categoriaOptenida.getNombreCategoria())){
            Categoria categoriaName=iCategoria.findByNombreCategoria(categoria.getNombreCategoria());
            if(categoriaName!=null){
                throw new RuntimeException("Ya existe una categoría con ese nombre");
            }
            return iCategoria.save(categoria);
        }else{
             categoriaOptenida.setDescripcionCategoria(categoria.getDescripcionCategoria());
             return iCategoria.save(categoriaOptenida);
        }
    }
   //listar platos de una categoria
    public List<Plato> listadoPlatos(String nombreCategoria){
        Categoria categoria=iCategoria.findByNombreCategoria(nombreCategoria);
        List<Plato>listadoPlatos=categoria.getPlatos();
        if(listadoPlatos.isEmpty()){
            throw new RuntimeException("Esa categoria no tiene platos");
        }
        return listadoPlatos;
    }

    public CategoriaService(ICategoria iCategoria) {
        this.iCategoria = iCategoria;
    }
}
