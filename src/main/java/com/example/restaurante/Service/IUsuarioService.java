package com.example.restaurante.Service;

import com.example.restaurante.Entity.Producto;
import com.example.restaurante.Entity.Usuario;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUsuarioService {

    public Usuario save(Usuario usuario);

    public Usuario findById(UUID ide);

    public List<Usuario> findAll();

    public int addProductoCarrito(UUID ideProducto, UUID ide, int cantidadComprar);

    public float calcularImporteCompra(List<Producto>productos);

}
