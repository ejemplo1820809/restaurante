package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface IUsuario extends JpaRepository<Usuario, UUID> {
}
