package com.example.restaurante.Controller;


import com.example.restaurante.DTO.PersonaDTO;
import com.example.restaurante.Entity.Persona;
import com.example.restaurante.Mapper.DTOPersona;
import com.example.restaurante.Service.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;


@RequestMapping("/persona")
@RestController
public class PersonaController {

    @Autowired
    private IPersonaService iPersonaService;

    @Autowired
    private DTOPersona dtoPersona;


    @PostMapping("/add")
    public ResponseEntity<HashMap<String, String>> save( @Valid @RequestBody PersonaDTO personaDTO){
        Persona persona=dtoPersona.convertiraPersona(personaDTO);
        persona=iPersonaService.save(persona);
        HashMap<String, String> datosPersona= new HashMap<>();
        datosPersona.put(persona.getNombre(), persona.getSolapin());
        return ResponseEntity.status(HttpStatus.OK).body(datosPersona);
    }
}
