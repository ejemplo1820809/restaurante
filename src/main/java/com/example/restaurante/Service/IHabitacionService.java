package com.example.restaurante.Service;


import com.example.restaurante.Entity.Habitacion;

public interface IHabitacionService {

    public Habitacion save (Habitacion habitacion);

}
