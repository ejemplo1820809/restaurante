package com.example.restaurante.Service;


import com.example.restaurante.Entity.Evento;
import com.example.restaurante.Entity.Persona;
import com.example.restaurante.Entity.Ponente;
import com.example.restaurante.Repository.IEventorepository;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EventoService implements IEventoService{

    @Autowired
    private IEventorepository iEventorepository;

    //salvar un evento
    public Evento save(Evento evento){
        return iEventorepository.save(evento);
    }

    //buscar un evento por el nombre
    public Evento findElementByID(UUID uuid){
        Optional<Evento> evento=iEventorepository.findById(uuid);
        if(!evento.isPresent()){
            throw new RuntimeException("Ese evento no se encuentra registrado");
        }
        return evento.get();
    }

    //buscar un evento por el nombre
    public Evento findElementByName(String name){
        Evento evento=iEventorepository.findByNombreEvento(name);
        if(evento==null){
            throw new RuntimeException("No se encuentra ese evento en la bD");
        }
        return evento;
    };

    //devolver la cantidad de participantes de un evento;
    public int cantidadParticipantes(String name){
        Evento evento=findElementByName(name);
        int cantidadParticipantes=evento.getPersonaList().size();
        return cantidadParticipantes;
    }

    //Devolver cantidad de participantes ponentes
    public int cantidadParticipantesPonentes(String name){
        Evento evento=findElementByName(name);
        List<Persona> listadoPersonas=evento.getPersonaList();
        int contadorPonente=0;
        for (Persona listadoPersona : listadoPersonas) {
            if (listadoPersona instanceof Ponente) {
                contadorPonente++;
            }
        }
        return contadorPonente;
    }
    //Devolver cantidad de dinero recaudado por un evento
    public double dineroRecaudado(String name){
        Evento evento=findElementByName(name);
        int cantidadParticipantes=cantidadParticipantes(name);
        return cantidadParticipantes*evento.getPrecio();
    }

    //Eliminar un evento;
    public String deleteById(String name){
        Evento evento=findElementByName(name);
        iEventorepository.deleteById(evento.getIdeEvento());
        String msj="Se eliminó de forma correcta";
        return msj;
    }

    public byte[] creandoExcelInformacion(String name) throws IOException {
        Evento evento=findElementByName(name);
        String nombreEvento= evento.getNombreEvento();
        System.out.print("Este es el nombre del evento"+nombreEvento);
        List<Persona>listadoPersonas=evento.getPersonaList();
        //Cantidad de participantes
        int cantidadParticipantes=listadoPersonas.size();
        System.out.println("Esta es la cantidad de personas que participaron en el evento "+cantidadParticipantes);
        //Cantidad de ponentes
        int cantidadPonentes=cantidadParticipantesPonentes(name);
        System.out.println("Esta es la cantidad de personas de ponentes "+cantidadPonentes);
        //Creando mi excel
        Workbook workbook = new XSSFWorkbook();

        //Hoja de todos los participantes
        Sheet sheetUno= workbook.createSheet("Participantes");
        //Creando la primera fila nombre del evento y otros datos
        Row row_uno= sheetUno.createRow(0);
        Cell cell_uno=row_uno.createCell(0);
        cell_uno.setCellValue("Nombre del evento: "+ nombreEvento);

        //Agregando una imagen
        FileInputStream fis = new FileInputStream("C:\\Users\\Llull\\Desktop\\Uciencia.png");
        byte[] imageBytes = IOUtils.toByteArray(fis);
        int pictureIdx = workbook.addPicture(imageBytes, Workbook.PICTURE_TYPE_PNG);
        fis.close();

        //Colocando la imagen
        CreationHelper helper = workbook.getCreationHelper();
        Drawing<?> drawing = sheetUno.createDrawingPatriarch();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(7);
        anchor.setCol1(13);// Columna donde se insertará la imagen
        anchor.setRow1(0);
        anchor.setRow2(7); // Fila donde se insertará la imagen

        Picture picture = drawing.createPicture(anchor, pictureIdx);
        picture.resize();
           // Cerrando de colocar la imagen
        Row row_dos= sheetUno.createRow(1);
        Cell cell_dos=row_dos.createCell(0);
        Date fecha =evento.getFechaEvento();
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        String fechaFormato=sdf.format(fecha);
        cell_dos.setCellValue("Fecha: "+ fechaFormato);

        //Iniciando tablas
        Row row_tres= sheetUno.createRow(2);
        Cell cell_tres=row_tres.createCell(0);
        cell_tres.setCellValue("Listado de participantes");

        Row rowText=sheetUno.createRow(4);
        Cell cellTextNombre=rowText.createCell(0);
        cellTextNombre.setCellValue("Nombre del participante");

        Cell cellTextapellido=rowText.createCell(1);
        cellTextapellido.setCellValue("Apellido");

        Cell cellTextsolapin=rowText.createCell(2);
        cellTextsolapin.setCellValue("Solapin");

        Cell cellTextInstitucion=rowText.createCell(3);
        cellTextInstitucion.setCellValue("Institucion");

        Cell cellTextCorreo=rowText.createCell(4);
        cellTextCorreo.setCellValue("Correo");

        int indexFila=5;
        for (Persona listadoPersona : listadoPersonas) {
            Row row = sheetUno.createRow(indexFila);
            Cell cellNombre = row.createCell(0);
            cellNombre.setCellValue(listadoPersona.getNombre());
            System.out.println("Este es el nombre"+listadoPersona.getNombre());

            Cell cellApellido = row.createCell(1);
            cellApellido.setCellValue(listadoPersona.getPrimerApellido());

            Cell cellSolapin = row.createCell(2);
            cellSolapin.setCellValue(listadoPersona.getSolapin());

            Cell cellInstitucion = row.createCell(3);
            cellInstitucion.setCellValue(listadoPersona.getInstitucion());

            Cell cellCorreo = row.createCell(4);
            cellCorreo.setCellValue(listadoPersona.getCorreo());
            indexFila++;
        }

        //Creando la segunda hoja
        Sheet sheetDos= workbook.createSheet("Ponentes");
        Row rowSheetD=sheetDos.createRow(0);
        Cell cellSheeetD= rowSheetD.createCell(0);
        cellSheeetD.setCellValue("Listado de ponentes ");

        int indexD=2;
        for (int i=0; i<cantidadParticipantes; i++){

            if(listadoPersonas.get(i) instanceof Ponente){
                Row row=sheetDos.createRow(indexD);
                Cell cellNombre=row.createCell(0);
                cellNombre.setCellValue(listadoPersonas.get(i).getNombre());

                Cell cellApellido=row.createCell(1);
                cellApellido.setCellValue(listadoPersonas.get(i).getPrimerApellido());

                Cell cellSolapin=row.createCell(2);
                cellSolapin.setCellValue(listadoPersonas.get(i).getSolapin());

                Cell cellInstitucion=row.createCell(3);
                cellInstitucion.setCellValue(listadoPersonas.get(i).getInstitucion());

                Cell cellCorreo=row.createCell(4);
                cellCorreo.setCellValue(listadoPersonas.get(i).getCorreo());
                indexD++;
            }

        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        byte[] excelBytes = outputStream.toByteArray();
        return excelBytes;
    }
}
