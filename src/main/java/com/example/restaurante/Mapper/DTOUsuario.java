package com.example.restaurante.Mapper;

import com.example.restaurante.DTO.UsuarioDTO;
import com.example.restaurante.Entity.Usuario;
import org.springframework.stereotype.Component;

@Component
public class DTOUsuario {

    public Usuario convertirUsuario(UsuarioDTO usuarioDTO){
        Usuario usuario=new Usuario();
        usuario.setNombre(usuarioDTO.getNombre());
        usuario.setPrimerApellido(usuarioDTO.getPrimerApellido());
        usuario.setCorreo(usuarioDTO.getCorreo());
        usuario.setTelefono(usuarioDTO.getTelefono());
        usuario.setDireccionParticular(usuarioDTO.getDireccion());
        return usuario;
    }


}
