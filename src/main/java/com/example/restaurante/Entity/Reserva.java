package com.example.restaurante.Entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="resrrvacion")
@Entity
public class Reserva implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(name = "nombre", nullable = false)
    private String nombreCompleto;

    @Email
    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private int numeroTelefono;

    @Column(nullable = false)
    private String direccion;

    @Column(nullable = false)
    private int camtidadPersona;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(nullable = false)
    private Date date;

    @DateTimeFormat(pattern = "HH:mm:ss")
    @Column(nullable = false)
    private LocalTime horaEspecifica;

    private String estado="Sin asignacion";

    private int numeroMesa=0;
}
