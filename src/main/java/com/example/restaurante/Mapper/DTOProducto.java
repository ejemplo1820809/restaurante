package com.example.restaurante.Mapper;


import com.example.restaurante.DTO.ProductoDTO;
import com.example.restaurante.Entity.Producto;
import org.springframework.stereotype.Component;

@Component
public class DTOProducto {

    public Producto convertirProducto(ProductoDTO productoDTO){
        Producto producto= new Producto();
        producto.setNombre(productoDTO.getNombre());
        producto.setPrecio(productoDTO.getPrecio());
        producto.setCantidadDisponible(productoDTO.getCantidadDisponible());
        return producto;
    }

}
