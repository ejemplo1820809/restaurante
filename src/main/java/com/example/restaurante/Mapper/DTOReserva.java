package com.example.restaurante.Mapper;


import com.example.restaurante.DTO.ReservaDTO;
import com.example.restaurante.Entity.Reserva;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DTOReserva {

    public Reserva convertirReservaDTO(ReservaDTO reservaDTO){
        Reserva reserva=new Reserva();
        reserva.setNombreCompleto(reservaDTO.getNombre());
        reserva.setEmail(reservaDTO.getCorreoElectronico());
        reserva.setNumeroTelefono(reservaDTO.getNumeroTelefono());
        reserva.setDireccion(reservaDTO.getDireccion());
        reserva.setDate(reservaDTO.getDate());
        reserva.setHoraEspecifica(reservaDTO.getHora());
        reserva.setCamtidadPersona(reservaDTO.getCantidadPersonas());
        return reserva;
    }

    public ReservaDTO convertirReserva(Reserva reserva){
        ReservaDTO reservaDTO=new ReservaDTO();
        reservaDTO.setNombre(reserva.getNombreCompleto());
        reservaDTO.setCorreoElectronico(reserva.getEmail());
        reservaDTO.setDireccion(reserva.getDireccion());
        reservaDTO.setNumeroTelefono(reserva.getNumeroTelefono());
        reservaDTO.setDate(reserva.getDate());
        reservaDTO.setHora(reserva.getHoraEspecifica());
        reservaDTO.setCantidadPersonas(reserva.getCamtidadPersona());
        reservaDTO.setEstado(reserva.getEstado());
        reservaDTO.setNumeroMesa(reserva.getNumeroMesa());
        return reservaDTO;
    }

    public List<ReservaDTO> convertirListaReserva(List<Reserva>listReserva){
        List<ReservaDTO>listado=new ArrayList<>();
        for (Reserva reserva : listReserva) {
            ReservaDTO reservaDTO = convertirReserva(reserva);
            listado.add(reservaDTO);

        }
        return listado;
    }

}
