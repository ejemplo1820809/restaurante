package com.example.restaurante.Service;


import com.example.restaurante.Entity.Producto;
import com.example.restaurante.Entity.Usuario;
import com.example.restaurante.Repository.IUsuario;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UsuarioService implements IUsuarioService{

    @Autowired
    private IUsuario iuser;

    @Autowired
    private IProductoService iproducto;



    public Usuario save(Usuario usuario){
        return iuser.save(usuario);
    }

    public Usuario findById(UUID uuid){
        Optional<Usuario> usuario=iuser.findById(uuid);
        if(!usuario.isPresent()){
            throw new RuntimeException("Ese usuario no existe");
        }
        return usuario.get();
    }

    public List<Usuario> findAll(){
        List<Usuario> usuarios=iuser.findAll();
        if(usuarios.isEmpty()){
            throw new RuntimeException("La lista de usuarios esta vacia");
        }
        return usuarios;
    }


    @Transactional
    public int addProductoCarrito(UUID ideProducto, UUID ide, int cantidadComprar){
        int cantidad=0;
        System.out.println("Entro");
        Usuario user=findById(ide);
        Producto producto=iproducto.findByID(ideProducto);
        producto.setCantidadComprar(cantidadComprar);
        Producto productoActualizado=iproducto.save(producto);
        if (!(productoActualizado.getCantidadDisponible() < productoActualizado.getCantidadComprar())) {
            throw new RuntimeException("No se puede agregar al carrito por falta de disponibilidad del producto");
        } else {
            user.getProductos().add(productoActualizado);
            Usuario usuarioActualizado= iuser.save(user);
            cantidad=usuarioActualizado.getProductos().size();
        }
        return cantidad;
    }

    public float calcularImporteCompra(List<Producto>productos){
        float importeFinal=0;
        if(productos.isEmpty()){
            throw new RuntimeException("La lista de archivos esta vacia");
        }
        for (Producto producto : productos) {
            int cantidadComprar = producto.getCantidadComprar();
            int cantidadDisponibles = producto.getCantidadDisponible();
            float precioproducto = producto.getPrecio();
            importeFinal += cantidadComprar * precioproducto;
            cantidadDisponibles = cantidadDisponibles - cantidadComprar;
            producto.setCantidadDisponible(cantidadDisponibles);
            iproducto.save(producto);
        }
        return importeFinal;
    }






}
