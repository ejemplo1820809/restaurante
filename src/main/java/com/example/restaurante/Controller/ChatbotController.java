package com.example.restaurante.Controller;


import com.google.cloud.dialogflow.v2.*;
import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequestMapping("/chatbot")
public class ChatbotController {

    @PostMapping("/consulta")
    public ResponseEntity<String> processUserRequest(@RequestBody String userMessage) {
        try{
            System.out.println("aaaaaaaaaa");
            SessionsClient sessionsClient = SessionsClient.create();
            // Aquí va la lógica para establecer la sesión con Dialogflow y enviar el mensaje del usuario
            String projectId = "chatbotprue-9cyr";
            String seccionId= "chatbot"+UUID.randomUUID().toString();
            SessionName session = SessionName.of(projectId, seccionId);

           //Hacer la consulta
            QueryInput queryInput = QueryInput.newBuilder()
                    .setText(com.google.cloud.dialogflow.v2.TextInput.newBuilder()
                            .setText(userMessage).setLanguageCode("es")).build();
            /*TextInput.Builder textInput = TextInput.newBuilder().setText(userMessage).setLanguageCode("es");
            QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();*/

            //Realiza la detección de intención
            DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);
            QueryResult queryResult=response.getQueryResult();


            // Aquí es donde puedes agregar la lógica del negocio basada en las intenciones y entidades detectadas
            String intent = queryResult.getIntent().getDisplayName();
            System.out.println("Esta es la intencion"+intent);
            if (intent.equals("Hacer una reserva")) {
                // Llamar a un servicio que procese el pedido
               System.out.println("Entrooooo");
                return ResponseEntity.status(HttpStatus.SC_OK).body("Hay reserva para");
            }
        }
        catch (Exception e) {
            return ResponseEntity.status(HttpStatus.SC_BAD_REQUEST).body("No hay reserva para") ;
        }
        return ResponseEntity.status(HttpStatus.SC_OK).body("Hay reserva para");
    }


}
