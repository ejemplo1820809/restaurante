package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Plato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface IPlato extends JpaRepository <Plato, UUID> {


    List<Plato> findByCategoria_NombreCategoria(String nombre);

    List<Plato> findByVegetariano(boolean vegetariano);

    List<Plato> findByActivo(boolean activo);
}
