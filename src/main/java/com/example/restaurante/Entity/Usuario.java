package com.example.restaurante.Entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Table(name="usuario")
public class Usuario implements Serializable {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(name="nombre", nullable = false)
    private String nombre;

    @Column(name="primerApellido", nullable = false)
    private String primerApellido;

    @Email
    @Column(name="correo", nullable = false, unique = true)
    private String correo;

    @Column(name="direccion", nullable = false)
    private String direccionParticular;

    @Column(name="telefono", nullable = false, unique = true)
    private String telefono;

    @ManyToMany
    @JoinTable(name="usuarioProducto", joinColumns = @JoinColumn(name = "producto"), inverseJoinColumns = @JoinColumn(name = "usuario"))
    private List<Producto> productos;

}
