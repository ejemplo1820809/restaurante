package com.example.restaurante.DTO;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UsuarioDTO {

    @NotNull
    private String nombre;

    @NotNull
    private String primerApellido;

    @NotNull
    private String telefono;

    @Email
    private String correo;

    @NotNull
    private String direccion;
}
