package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface IHotelRepository extends JpaRepository<Hotel, UUID> {

    //Aqui estoy usando la consulta nombrada en la propia entidad hotel que en este caso
    //recibe un parametro que ese parametro debe ser igual al que
    public List<Hotel> findByCategoria(String name);

    //Aqui estoy creando una consulta personalizada
    public List<Hotel> findByLocalidad(String name);

    public Hotel findByNombreEquals(String name);
}
