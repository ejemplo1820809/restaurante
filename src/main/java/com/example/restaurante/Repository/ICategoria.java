package com.example.restaurante.Repository;

import com.example.restaurante.Entity.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface ICategoria extends JpaRepository<Categoria, UUID> {

    Categoria findByNombreCategoria(String nombre);

    List<Categoria> findByPlatosIsNotNull();

}
