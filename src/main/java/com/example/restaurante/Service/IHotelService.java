package com.example.restaurante.Service;

import com.example.restaurante.Entity.Habitacion;
import com.example.restaurante.Entity.Hotel;
import org.springframework.data.domain.Page;

import java.awt.print.Pageable;
import java.util.List;
import java.util.UUID;

public interface IHotelService {

    //Para paginar hoteles
    public Page<Hotel> findAll(int page1, int page2);

    public List<Hotel> obtenerHotelOrdenado();

    public Hotel save(Hotel hotel);

    public List<Hotel> findByCategoria(String name);

    public List<Hotel> findByLocalidad(String name);

    public void AsignarHabitacion(UUID ideHabi, String nombreHotel);

    public List<Habitacion> listadohabitaciones(String name);
}
