package com.example.restaurante.Service;


import com.example.restaurante.Entity.Producto;
import com.example.restaurante.Repository.IProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class ProductoService implements IProductoService{
    @Autowired
    private IProducto iProducto;

    public Producto save(Producto producto){
        return iProducto.save(producto);
    }

    public Producto findByID(UUID ide){
        Optional<Producto> producto=iProducto.findById(ide);
        if(!producto.isPresent()){
            throw new RuntimeException("El producto no se encuentra");
        }
        return producto.get();
    }

}
