package com.example.restaurante.DTO;


import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
public class ProductoDTO {

    @NotNull
    private String nombre;

    @NotNull
    private float precio;

    @NotNull
    private int cantidadDisponible;


}
