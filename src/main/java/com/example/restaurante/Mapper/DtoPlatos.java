package com.example.restaurante.Mapper;


import com.example.restaurante.DTO.PlatoDTO;
import com.example.restaurante.Entity.Categoria;
import com.example.restaurante.Entity.Plato;
import com.example.restaurante.Repository.ICategoria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DtoPlatos {

    @Autowired
    private ICategoria icategoria;

    public Plato convertirPlatoDTO(PlatoDTO platoDTO){
        Plato plato=new Plato();
        plato.setNombre(platoDTO.getNombre()); //nombre del plato
        plato.setIngredientes(platoDTO.getIngredientes()); //ingredientes del plato
        plato.setPrecioReal(platoDTO.getPrecioReal()); //precio real
        plato.setDescuento(platoDTO.getDescuento()); //descuento
        plato.setActivo(platoDTO.isActivo()); //activo
        plato.setVegetariano(platoDTO.isVegetariano()); //vegetariano
        plato.setImagen(platoDTO.getImagen()); //imagen
        //Buscando la categoria
        Categoria categoria=icategoria.findByNombreCategoria(platoDTO.getNombreCategoria());
        plato.setCategoria(categoria); //categoria
        return plato;
    }

    public PlatoDTO convertirPlato(Plato plato){
        PlatoDTO platoDTO= new PlatoDTO();
        platoDTO.setNombre(plato.getNombre()); //nombre del plato
        platoDTO.setIngredientes(plato.getIngredientes()); //ingredientes del plato
        platoDTO.setPrecioReal(plato.getPrecioReal());//precio real del plato
        platoDTO.setDescuento(plato.getDescuento()); //descuento del plato
        platoDTO.setActivo(plato.isActivo()); //si es activo
        platoDTO.setVegetariano(plato.isVegetariano()); //si es vegetariano
        platoDTO.setImagen(plato.getImagen()); //la imagen
        platoDTO.setNombreCategoria(plato.getCategoria().getNombreCategoria());//categoria
        return platoDTO;
    }


    //este mapper lo cree para cuando quiera listar los platos de una categoria no me muestre el nombre
    public PlatoDTO convertirPlatoCategoria(Plato plato){
        PlatoDTO platoDTO= new PlatoDTO();
        platoDTO.setNombre(plato.getNombre()); //nombre del plato
        platoDTO.setIngredientes(plato.getIngredientes()); //ingredientes del plato
        platoDTO.setPrecioReal(plato.getPrecioReal());//precio real del plato
        platoDTO.setDescuento(plato.getDescuento()); //descuento del plato
        platoDTO.setActivo(plato.isActivo()); //si es activo
        platoDTO.setVegetariano(plato.isVegetariano()); //si es vegetariano
        platoDTO.setImagen(plato.getImagen()); //la imagen
        return platoDTO;
    }

    public List<PlatoDTO> convertirListPlato(List<Plato> platos){
        List<PlatoDTO> listadoPlatosDTO= new ArrayList<>();
        for (Plato plato: platos) {
            PlatoDTO platoDTO=convertirPlato(plato);
            listadoPlatosDTO.add(platoDTO);
        }
        return listadoPlatosDTO;
    }

    public List<PlatoDTO> convertirListPlatoCategoria(List<Plato> platos){
        List<PlatoDTO> listadoPlatosDTO= new ArrayList<>();
        for (Plato plato: platos) {
            PlatoDTO platoDTO=convertirPlatoCategoria(plato);
            listadoPlatosDTO.add(platoDTO);
        }
        return listadoPlatosDTO;
    }
}
