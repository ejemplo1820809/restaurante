package com.example.restaurante.Mapper;


import com.example.restaurante.DTO.PersonaDTO;
import com.example.restaurante.Entity.Persona;
import com.example.restaurante.Entity.Ponente;
import org.springframework.stereotype.Component;

@Component
public class DTOPersona {

    public Persona convertiraPersona(PersonaDTO personaDTO){
        String nombre=personaDTO.getNombre();
        String primerApellido=personaDTO.getPrimerApellido();
        String solapin= personaDTO.getSolapin();
        String institucion=personaDTO.getInstitucion();
        String correo= personaDTO.getCorreo();
        String nombreEvento=personaDTO.getNombreEvento();
        if(personaDTO.getTituloPonencia()!=null){
            String tituloOpenencia=personaDTO.getTituloPonencia();
            Ponente ponente=new Ponente();
            ponente.setNombre(nombre);
            ponente.setPrimerApellido(primerApellido);
            ponente.setSolapin(solapin);
            ponente.setInstitucion(institucion);
            ponente.setCorreo(correo);
            ponente.setTituloPonencia(tituloOpenencia);
            ponente.setNombreEvento(nombreEvento);
            return ponente;
        }else{
            Persona persona= new Persona();
            persona.setNombre(nombre);
            persona.setPrimerApellido(primerApellido);
            persona.setSolapin(solapin);
            persona.setInstitucion(institucion);
            persona.setCorreo(correo);
            persona.setNombreEvento(nombreEvento);
            return persona;
        }
    }
}
