package com.example.restaurante.Controller;


import com.example.restaurante.DTO.ReservaDTO;
import com.example.restaurante.Entity.Reserva;
import com.example.restaurante.Mapper.DTOReserva;
import com.example.restaurante.Service.IReservaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/reserva")
@RestController
public class ReservaController {

    @Autowired
    private IReservaService iReservaService;

    @Autowired
    private DTOReserva dtoReserva;


    @PostMapping("/add")
    public ResponseEntity<ReservaDTO> save(@Valid @RequestBody ReservaDTO reservaDTO){
        Reserva reserva=dtoReserva.convertirReservaDTO(reservaDTO);
        Reserva reservaAlmacenada=iReservaService.save(reserva);
        ReservaDTO reservaDTO1= dtoReserva.convertirReserva(reservaAlmacenada);
       return ResponseEntity.status(HttpStatus.OK).body(reservaDTO1);
    }

}
