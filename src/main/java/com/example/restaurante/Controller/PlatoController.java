package com.example.restaurante.Controller;


import com.example.restaurante.DTO.PlatoDTO;
import com.example.restaurante.Entity.Plato;
import com.example.restaurante.Mapper.DtoPlatos;
import com.example.restaurante.Service.IPlatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


@RequestMapping("/platos")
@RestController
public class PlatoController {

    @Autowired
    private IPlatoService iPlatoService;

    @Autowired
    private DtoPlatos dtoPlatos;

    @PostMapping(value="/add", consumes = {"multipart/form-data"})
    public ResponseEntity<PlatoDTO>save(@RequestPart("imagen") MultipartFile imagen, @Valid @RequestPart("platoDTO") PlatoDTO platoDTO) throws IOException {
        byte[] imagenA=imagen.getBytes();
        platoDTO.setImagen(imagenA);
        Plato plato= dtoPlatos.convertirPlatoDTO(platoDTO);
        Plato platoSalvado=iPlatoService.save(plato);
        return ResponseEntity.status(HttpStatus.OK).body(dtoPlatos.convertirPlato(platoSalvado));
    }


    @DeleteMapping("/deleteAll")
    public ResponseEntity<String> deleteAll(){
        iPlatoService.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).body("Se eliminaron de forma correcta todos los platos");
    }

    @DeleteMapping("delete/{ide}")
    public ResponseEntity<String> deleteByID(@PathVariable UUID ide){
        iPlatoService.deleteById(ide);
        return ResponseEntity.status(HttpStatus.OK).body("Se eliminó el plato de forma correcta");

    }

    @GetMapping("/buscar/{ide}")
    public ResponseEntity<PlatoDTO> findById(@PathVariable UUID ide){
        Plato plato=iPlatoService.findById(ide);
        PlatoDTO platoDTO=dtoPlatos.convertirPlato(plato);
        return ResponseEntity.status(HttpStatus.OK).body(platoDTO);
    }

    @PutMapping(value = "/update/{ide}", consumes = {"multipart/form-data"} )
    public ResponseEntity<PlatoDTO> update(@RequestPart("imagen") MultipartFile imagen, @PathVariable  UUID ide, @RequestPart("platoDTO") PlatoDTO platoDTO) throws IOException {
        byte[] imagenB=imagen.getBytes();
        platoDTO.setImagen(imagenB);
        Plato plato=dtoPlatos.convertirPlatoDTO(platoDTO);
        Plato platoA=iPlatoService.update(ide, plato);
        return ResponseEntity.status(HttpStatus.OK).body(dtoPlatos.convertirPlato(platoA));

    }

    @GetMapping("/listar")
    public ResponseEntity<List<PlatoDTO>> findAll(){
        List<Plato> listadoPlatos=iPlatoService.findAll();
        List<PlatoDTO> listadoPlatosDTO= dtoPlatos.convertirListPlato(listadoPlatos);
        return ResponseEntity.status(HttpStatus.OK).body(listadoPlatosDTO);
    }

    @GetMapping("/listar/categoria")
    public ResponseEntity<List<PlatoDTO>> findCategoria(@RequestParam String nombreCategoria){
        List<Plato>listadoPlatos=iPlatoService.findByCategoria(nombreCategoria);
        List<PlatoDTO>listadoPlatosDTO=dtoPlatos.convertirListPlato(listadoPlatos);
        return ResponseEntity.status(HttpStatus.OK).body(listadoPlatosDTO);
    }

    @GetMapping("/listar/activo")
    public ResponseEntity<List<PlatoDTO>> findActivo(@RequestParam boolean activo){
        List<Plato>listadoPlatos=iPlatoService.findBtActivo(activo);
        List<PlatoDTO>listadoPlatosDTO=dtoPlatos.convertirListPlato(listadoPlatos);
        return ResponseEntity.status(HttpStatus.OK).body(listadoPlatosDTO);
    }

    @GetMapping("/listar/vegetariano")
    public ResponseEntity<List<PlatoDTO>> findVegetariano(@RequestParam boolean vegetariano){
        List<Plato>listadoPlatos=iPlatoService.findByVegetariana(vegetariano);
        List<PlatoDTO>listadoPlatosDTO=dtoPlatos.convertirListPlato(listadoPlatos);
        return ResponseEntity.status(HttpStatus.OK).body(listadoPlatosDTO);
    }

    @GetMapping("/pdf")
    public ResponseEntity CrearPDF() throws Exception {
        try {
            byte[] a = iPlatoService.CrearCarta();
            ByteArrayResource resource = new ByteArrayResource(a);

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=carta.pdf")
                    .header(HttpHeaders.CONTENT_TYPE, "application/pdf")
                    .header(HttpHeaders.SET_COOKIE, "fileDownload=true; path=/")
                    .body(resource);
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
        }
    }

}
